JSHint
======

What Is It?
-----------

The [JSHint website](http://jshint.com/about/) says it best: "JSHint is a community-driven tool to detect errors and potential problems in JavaScript code and to enforce your team's coding conventions."

We started with [JQuery's linting guidelines] but made some changes to the [options](http://jshint.com/docs/options/). For example, preference for spaces instead of tabs and single quotes instead of double quotes. 

We have files for different environments. One is for JavaScript running in the browser, the other targeting JavaScript running in Node.js. 

When Do I Use It?
-----------------

As of Friday, March 21, 2014, we have no formal processes around linting. But we encourage you to include the appropriate `.jshintrc` file in your project's root directory. (Remember to remove the "browser" or "node" prefix so you are left with a dotfile. But you knew that.)

If your project is scaffolded with [Yeoman](http://yeoman.io/) to use [Grunt](http://gruntjs.com/), then the registered Grunt JSHint task will use these new defaults. 

If your project isn't scaffolded, then you should:

1. Install Node.js
2. Install JSHint globally using `npm install -g jshint`
3. Run `jshint file_name.js` as you work and before you deploy
4. Revel in the sense of completion that successful linting affords you
